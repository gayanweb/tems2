import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '@angular/material';

import { NotificationService } from './notification.service'

import {MainComponent} from './main.component'

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  // entryComponents: [MainComponent],
  declarations: [MainComponent],
  providers: [NotificationService],
  bootstrap: [MainComponent]
})
export class NotificationModule { }
