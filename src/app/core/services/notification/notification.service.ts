import {Injectable} from '@angular/core';
import {MdSnackBar, MdSnackBarConfig, MdSnackBarRef} from '@angular/material/snack-bar'
import {MdDialog} from '@angular/material/dialog'

import {MainComponent}  from './main.component'

@Injectable()
export class NotificationService {
  private defaultConfig: MdSnackBarConfig;

  constructor(private snackbar: MdSnackBar) {
    this.init();
  }

  /**
   * Init default configurations
   */
  public init(){
    this.defaultConfig = new MdSnackBarConfig();
    this.defaultConfig.duration = 3000;
  }

  /**
   *
   * @param message Message to be throw
   */
  public error(message: string) {

    let config = new MdSnackBarConfig();
    config.duration = 3000;

    this.throwMessage(message, config);

  }

  public info(message: string) {
    this.throwMessage(message, this.defaultConfig);
  }

  public success(message: string) {
    this.throwMessage(message, this.defaultConfig);
  }

  public warning(message: string) {
    this.throwMessage(message, this.defaultConfig);
  }

  public throwMessage(message: string, config: MdSnackBarConfig) {

    let snackbar = this.snackbar.open(message, 'OK', config);
    snackbar.afterDismissed().subscribe(res => {
      console.log(res);
    })
  }

}
