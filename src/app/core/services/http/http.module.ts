import { NgModule } from '@angular/core';
import { HttpModule as AngularHttp, ConnectionBackend, XHRBackend } from '@angular/http';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    AngularHttp
  ],
  declarations: []
})
export class HttpModule {  }
