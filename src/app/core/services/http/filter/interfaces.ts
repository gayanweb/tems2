/**
 * Created by gayanyapa on 12/27/16.
 */
import { RequestOptionsArgs as AngularRequestOptionsArgs } from '@angular/http'

export interface RequestFilter{
  field: string,
  nullable?: boolean,
  operator: number,
  value: any
}

export interface Sort{
  field: string,
  order: string,
  priority: number
}

export interface RequestOptionsArgs extends AngularRequestOptionsArgs{

  filters?: Array<RequestFilter>
  sorts?: Array<Sort>
  fields?: Array<string>

}
