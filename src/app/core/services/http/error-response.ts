/**
 * Created by gayanyapa on 2/5/17.
 */
import {ErrorResponse as Base, Error} from "./interfaces";
import {Response} from "@angular/http";
import {isArray} from "util";

export class ErrorResponse implements Base{
  private ref: any;

  constructor(private _response: Response){}

  public response(): Array<Error> {
    let response = this._response.json()['errors'];

    if(!isArray(response)){
      // throw 'Response data should be an array';
    }

    let errors = [];
    for(let idx in response){
      errors.push({
        code: response[idx].code,
        message: response[idx].message,
      });
    }

    console.log(errors);
    return errors;
  }

  setRef(ref: any){
    this.ref = ref;
  }

  getRef(){
    return this.ref;
  }

  getStatus(){
    return this._response.status;
  }

}
