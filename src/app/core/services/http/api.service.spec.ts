/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Http } from './http';

describe('ApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Http]
    });
  });

  it('should ...', inject([Http], (service: Http) => {
    expect(service).toBeTruthy();
  }));
});
