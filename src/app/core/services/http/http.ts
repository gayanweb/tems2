import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {
  Http as AngularHttp,
  Response,
  Request,
  RequestOptions,
  ConnectionBackend,
  Headers
} from '@angular/http'

import { RequestOptionsArgs } from './filter/interfaces'

import {isUndefined} from "util";

@Injectable()
export class Http extends AngularHttp {

  /*constructor(protected xxx: ConnectionBackend, protected yyy: RequestOptions) {
    super(xxx, yyy);
  }*/


  get(url: string, options?: RequestOptionsArgs): Observable<Response> {

    if(!isUndefined(options) && !isUndefined(options.filters)){

      options.headers =  isUndefined(options.headers) ? new Headers() : options.headers;

      options.headers.append('filters', JSON.stringify(options.filters));

    }

    return super.get(url, options);
  }

  post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return super.post(url, body, options);
  }

  put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return super.put(url, body, options);
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return super.delete(url, options);
  }

}
