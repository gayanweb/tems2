import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputComboComponent } from './input-combo.component';
import { PopupComponent } from './popup/popup.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@angular/material'
import { FormModule } from '../form/form.module'
import { AgGridModule } from 'ag-grid-ng2'
import { Http } from "../../services/http/http";
import {FilterModule} from "../filter/filter.module";

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    FormModule,
    AgGridModule.withComponents(['enableColResize']),
    FilterModule
  ],
  declarations: [InputComboComponent, PopupComponent],
  entryComponents: [InputComboComponent, PopupComponent],
  exports: [InputComboComponent, PopupComponent],
  providers: [Http],
})
export class InputComboModule { }
