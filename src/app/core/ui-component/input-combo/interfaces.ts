import { DataSource } from "../data-grid/interfaces";
import {MdDialogConfig, ComponentType} from "@angular/material";
import {Field} from "../filter/interfaces";
/**
 * Created by gayanyapa on 2/12/17.
 */



export declare class ComboPopupOptions{
  filters?: Array<Field>;
  dialogOptions?: MdDialogConfig;
  newComponentRef?: ComponentType<any>;
  dataSource?: DataSource;
}

export interface PopupField{
  name: string,
  field: string
}

export interface ComboOptions{
  fields:Array<PopupField>;
  keyParam: string,
  valParam: string,
  newComponentRef?: ComponentType<any>,
  title?: string,
  onSelect?(selected),
  filters?:Array<any>,
  dataSource?: DataSource,
  popupOptions?: ComboPopupOptions
}
