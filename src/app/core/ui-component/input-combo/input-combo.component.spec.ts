/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { InputComboComponent } from './input-combo.component';

describe('InputComboComponent', () => {
  let component: InputComboComponent;
  let fixture: ComponentFixture<InputComboComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputComboComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputComboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
