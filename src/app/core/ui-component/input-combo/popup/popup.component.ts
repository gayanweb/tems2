import {Component, OnInit, AfterViewInit, AfterContentInit, OnDestroy, Input} from '@angular/core';
import {GridOptions} from 'ag-grid/main'
import {Http} from "../../../services/http/http";
import {Observable, Subscription} from "rxjs";
import {Response} from "@angular/http";
import {Field, FieldType, Filter} from "../../filter/interfaces";
import {MdDialogRef, MdDialog, MdDialogConfig} from "@angular/material";
import {ComboPopupOptions} from "../interfaces";
import {isNullOrUndefined} from "util";
import {RequestFilter} from "../../../services/http/filter/interfaces";

@Component({
  selector   : 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls  : ['./popup.component.scss']
})
export class PopupComponent implements OnInit, AfterViewInit, AfterContentInit, OnDestroy {

  private _loading = false;

  private gridOptions: GridOptions;

  @Input()
  private options: ComboPopupOptions;

  private rowData: Array<any> = [];

  private filters: Array<Filter> = [];

  private selectedFilters: Array<RequestFilter> = [];

  private filterFields: Array<Field> = [
    {name: 'name', visibleName: 'Name', type: FieldType.TEXT},
    {name: 'id', visibleName: 'Id', type: FieldType.NUMBER},
    {name: 'date', visibleName: 'Date', type: FieldType.DATE},
  ];
  private subscriptions: Array<Subscription> = [];

  private popupOptions: ComboPopupOptions;

  constructor(private http: Http, private dialogRef: MdDialogRef<PopupComponent>, private dialog: MdDialog) {
    this.popupOptions = dialogRef.config.data;
  }

  ngOnInit() {
    this.setupGrid();


  }

  setupGrid(){
    this.gridOptions = {
      suppressLoadingOverlay   : true,
      rowSelection             : 'single',
      suppressRowClickSelection: false,
      rowHeight                : 40,
      animateRows              : true,
      onGridReady              : () => {
        this.gridOptions.api.sizeColumnsToFit();
      },
      onSelectionChanged       : () => {
        // this.selectedRowCount = this.gridOptions.api.getSelectedNodes().length;
      },
      columnDefs               : [
        {field: 'id', headerName: 'Id'},
        {field: 'name', headerName: 'Name'}
      ]
    };
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  ngAfterViewInit() {
//    this.loadData(this.selectedFilters);
  }

  ngAfterContentInit() {
    this.loadData(this.selectedFilters);
  }


  loading() {
    this._loading = true;
  }

  loaded() {
    this._loading = false;
  }

  loadData(filters) {
    this.loading();
    return this.subscriptions.push(this.popupOptions.dataSource.read(filters)
      .catch((error: Response | any) => {
        return Observable.throw('error');
      }).finally(() => {
        this.loaded();

      }).subscribe(data => {
        this.rowData = data;
        let observable = new Observable(_ => {
          setTimeout(_ => this.selectFirstRow());
        });
        this.subscriptions.push(observable.subscribe());
    }));
  }

  onKeyChange(event: KeyboardEvent) {
    console.log(event);
    if (event.key == 'ArrowDown' || event.key == 'ArrowUp' || event.key == 'Enter') {
      let direction = event.key === 'ArrowDown' ? 'down' : 'up';
      this.shiftSelectedRow(direction);
      if (event.key == 'Enter') {
        this.apply();
      }
    }
  }

  shiftSelectedRow(direction = 'down') {
    if (this.gridOptions.api.getSelectedNodes().length < 1) {
      this.selectFirstRow();
      return;
    }

    let selectedNode = this.gridOptions.api.getSelectedNodes()[0];
    let selectedNodeIdx = selectedNode.rowIndex;
    let shouldSelectedIdx = direction === 'down' ? selectedNodeIdx + 1 : selectedNodeIdx - 1;

    if (shouldSelectedIdx < 0) {
      return;
    }

    if (shouldSelectedIdx > this.gridOptions.api.getRenderedNodes().length - 1) {
      shouldSelectedIdx = 0;
    }

    let shouldSelected = this.gridOptions.api.getRenderedNodes()[shouldSelectedIdx];
    this.gridOptions.api.selectNode(shouldSelected);
  }

  selectFirstRow() {
    if (this.gridOptions.api.getRenderedNodes().length > 0) {
      let firstNode = this.gridOptions.api.getRenderedNodes()[0];
      this.gridOptions.api.selectNode(firstNode);
    }
  }

  apply() {
    if (this.gridOptions.api.getSelectedNodes().length < 1) {
      return;
    }

    let selectedNode = this.gridOptions.api.getSelectedNodes()[0];
    this.dialogRef.close(selectedNode.data);
  }

  cancel() {
    this.dialogRef.close(null);
  }

  createNew(){

    if(isNullOrUndefined(this.popupOptions.newComponentRef)){
      throw new Error('newComponentRef is required');
    }

    let config = new MdDialogConfig();
    config.width = '80%';
    config.height = '80%';
    config.position = {
      left: '10%',
      top: '10%'
    };

    let dialogRef = this.dialog.open(this.popupOptions.newComponentRef, config);
    this.subscriptions.push(dialogRef.afterClosed().subscribe(_ => {
      this.loadData(this.selectedFilters)
    }));

  }

  onFilterChange(filter: Filter){
    let existingIdx = this.filters.findIndex((object: Filter) => {
      return object.filter.field === filter.filter.field &&
        object.filter.operator === filter.filter.operator;
    });
    if (existingIdx > -1) {
      this.filters.splice(existingIdx ,1);
    }

    this.filters.push(filter);

    let requestFilters: Array<RequestFilter> = this.filters.map((filter) => {
      return filter.filter;
    });

    let requestFiltersCleared: Array<RequestFilter> = requestFilters.filter((filter) => {
      return filter.value != '';
    });

    this.selectedFilters = requestFiltersCleared;

    this.loadData(requestFiltersCleared)
  }

  removeFilter(filter: Filter) {
    this.filters.splice(this.filters.indexOf(filter), 1);
    this.loadData(this.selectedFilters)
  }

}
