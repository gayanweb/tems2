import {Component, OnInit, AfterContentInit, AfterViewInit, AfterContentChecked, OnDestroy, Input} from '@angular/core';
import {AgEditorComponent} from "ag-grid-ng2";
import {MdDialogConfig, MdDialog} from "@angular/material";
import {BrokerComponent} from "../../../setup/broker/broker.component";
import {PopupComponent} from "./popup/popup.component";
import {isNullOrUndefined} from "util";
import {Observable, Subscription} from "rxjs";
import {DataSource} from "../data-grid/interfaces";
import {ComboOptions, ComboPopupOptions} from "./interfaces";

@Component({
  selector: 'input-combo',
  templateUrl: './input-combo.component.html',
  styleUrls: ['./input-combo.component.scss']
})
export class InputComboComponent implements OnInit, OnDestroy, AfterViewInit {

  protected selectedObject: any = undefined;

  protected subscriptions: Array<Subscription> = [];

  @Input()
  public options: ComboOptions;

  constructor(protected dialog: MdDialog) { }

  ngOnInit() {
    if(isNullOrUndefined(this.options)){
      throw 'Input combo options are required';
    }
  }

  ngOnDestroy(){
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  ngAfterViewInit(){}

  openPopup() {

    if(!isNullOrUndefined(this.options.popupOptions)){
      this.setupPopConfig();
    }

    console.log(PopupComponent);

    let dialogRef = this.dialog.open(PopupComponent, this.options.popupOptions.dialogOptions);
    this.subscriptions.push(dialogRef.afterClosed().subscribe(data => {
      this.selectedObject = data;
      if(!isNullOrUndefined(this.options.onSelect)){
        this.selectedObject = data;
        this.options.onSelect(data)
      }
    }));
  }

  private setup(){

  }

  private setupPopConfig(){

    if(isNullOrUndefined(this.options.popupOptions)){
      throw new Error('Popup options required');
    }

    let config = new MdDialogConfig();
    config.height = '70%';
    config.width  = '70%';

    if(isNullOrUndefined(this.options.popupOptions.dialogOptions)){
      this.options.popupOptions.dialogOptions = config;
    }else {
      this.options.popupOptions.dialogOptions = Object.assign(config, this.options.popupOptions.dialogOptions);
    }

    //pass popup options to popup component
    this.options.popupOptions.dialogOptions.data = this.options.popupOptions

  }





}
