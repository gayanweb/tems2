import {Component, OnInit, ElementRef, trigger, transition, style, animate, state} from '@angular/core';

export enum Type{
  Success = 1,
  Error = 2,
  Warning = 3,
  Info = 4
}

export interface Notification{
  type: Type;
  message: string | ElementRef,
  onClick?(),
  inActive?: boolean
}

@Component({
  selector: 'toolbar-notify',
  templateUrl: './notify.component.html',
  styleUrls: ['./notify.component.scss'],
  animations: [
    /*trigger('itemInactive', [
      state('false', style({transform: 'translateX(0)', color: 'red'})),
      state('true', style({transform: 'translateX(0)', color: 'black'})),
      transition('false => true', [
        style({
//          transform: 'translateY(-100%)',
          opacity: 1, zoom: 1.1}),
        animate('1s ease-in')
      ]),
      transition('* => void', [
        animate('0.3s 10 ease-out', style({transform: 'translateY(-100%)', opacity: 0}))
      ])
    ])*/
    /*trigger('item', [
      state('in', style({transform: 'translateX(0)'})),
      transition('void => *', [
        style({transform: 'translateX(+100%)', opacity: 0}),
        animate('0.3s ease-in')
      ]),
      transition('* => void', [
        animate('0.3s 10 ease-out', style({transform: 'translateX(100%)', opacity: 0}))

      ])
    ])*/
  ]
})
export class NotifyComponent implements OnInit {

  private selected: string;

  private count: any = {
    activity: 3,
    notification: 8,
  };

  public notifications: Array<Notification> = [];

  public activities: Array<Notification> = [];

  constructor() { }

  whenMenuOpen(){
    this.notifications = [
      {inActive:false, type: Type.Success, message: 'Purchase order 222 has been created', onClick: () => { alert('Hi...')}},
      {inActive:false, type: Type.Error, message: 'Purchase order 222 has been created', onClick: () => { alert('Hi...')}},
      {inActive:false, type: Type.Info, message: 'Purchase order 222 has been created', onClick: () => { alert('Hi...')}},
      {inActive:false, type: Type.Success, message: 'Purchase order 222 has been created', onClick: () => { alert('Hi...')}},
      {inActive:false, type: Type.Info, message: 'Purchase order 222 has been created', onClick: () => { alert('Hi...')}},
      {inActive:false, type: Type.Success, message: 'Purchase order 222 has been created', onClick: () => { alert('Hi...')}},
      {inActive:false, type: Type.Warning, message: 'Purchase order 222 has been created', onClick: () => { alert('Hi...')}},
      {inActive:false, type: Type.Success, message: 'Purchase order 222 has been created', onClick: () => { alert('Hi...')}},
    ];

    this.activities = [
      {inActive:false, type: Type.Success, message: 'Purchase order 222 has been created', onClick: () => { alert('Hi...')}},
      {inActive:false, type: Type.Error, message: 'Purchase order 222 has been created', onClick: () => { alert('Hi...')}},
      {inActive:false, type: Type.Info, message: 'Purchase order 222 has been created', onClick: () => { alert('Hi...')}},
    ]
  }

  ngOnInit() {
//    this.count.activity = this.activities.length;
//    this.count.notification = this.notifications.length;
  }

  markInactive(notification: Notification){
    let idx = this.notifications.indexOf(notification);
    if(this.notifications[idx].inActive){
      return;
    }
    this.notifications[idx].inActive = true;
    this.count.notification --;

  }

  markActivityInactive(activity: Notification){
    let idx = this.activities.indexOf(activity);
    if(this.activities[idx].inActive){
      return;
    }
    this.activities[idx].inActive = true;
    this.count.activity --;

  }

  remove(notification: Notification){
    let idx = this.notifications.indexOf(notification);
    this.notifications.splice(idx, 1);
  }

  removeActivity(activity: Notification){
    let idx = this.activities.indexOf(activity);
    this.activities.splice(idx, 1);
  }

}
