import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotifyComponent } from './notify/notify.component';
import {MaterialModule} from "@angular/material";
import { FlexLayoutModule } from "@angular/flex-layout"

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule
  ],
  declarations: [NotifyComponent],
  entryComponents: [NotifyComponent],
  exports: [NotifyComponent]
})
export class ToolbarModule { }
