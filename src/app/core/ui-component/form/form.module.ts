import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout'
import { MdProgressSpinnerModule } from '@angular/material/progress-spinner'

import { LoaderComponent } from './loader/loader.component'


@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FlexLayoutModule,
    MdProgressSpinnerModule.forRoot()
  ],
  declarations: [LoaderComponent],
  exports: [LoaderComponent]
})
export class FormModule { }
