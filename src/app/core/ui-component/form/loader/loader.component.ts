import { Component, OnInit, AfterViewInit, ElementRef, ViewContainerRef, HostBinding, HostListener, Renderer, AfterContentInit } from '@angular/core';

@Component({
  selector: 'form-loader',

  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit, AfterViewInit {

  /*@HostBinding('style.height.px')
  elHeight:number = 0;*/

  /*@HostBinding('style.width.px')
  elWidth:number = 0;*/

  constructor(
    private element: ElementRef,
    private container: ViewContainerRef,
    private renderer: Renderer
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    // console.log(this.element);

    // this.resizeWithParent();

    // console.log(this.element.nativeElement.parentNode.clientHeight);
    // console.log(this.container)
  }

  ngAfterContentInit(){
    this.resizeWithParent();
  }

  resizeWithParent(){
    // console.log(this.element);
    let elHeight = this.element.nativeElement.parentNode.clientHeight;
    let elWidth = this.element.nativeElement.parentNode.clientWidth;
    this.renderer.setElementStyle(this.container.element.nativeElement, 'height', `${elHeight}px`);
    this.renderer.setElementStyle(this.container.element.nativeElement, 'width', `${elWidth}px`);
    // this.elHeight = this.element.nativeElement.parentNode.clientHeight;
    // this.elWidth = this.element.nativeElement.parentNode.clientWidth;
  }

  @HostListener('window:resize', ['$event.target'])
  onResize() {
    this.resizeWithParent();
  }

}
