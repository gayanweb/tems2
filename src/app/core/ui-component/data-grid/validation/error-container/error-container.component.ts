import { Component, OnInit, Input, trigger, state, transition, style, animate } from '@angular/core';

@Component({
  selector: 'grid-error-container',
  templateUrl: './error-container.component.html',
  styleUrls: ['./error-container.component.scss'],
  animations: [
    trigger('onError', [
      transition('void => *', [
        style({transform: 'translateY(-100%)', opacity: 0}),
        animate('0.3s ease-in')
      ]),
      transition('* => void', [
        animate('0.3s 10 ease-out', style({transform: 'translateY(-100%)', opacity: 0}))
      ])
    ])
  ]
})
export class ErrorContainerComponent implements OnInit {

  @Input()
  public errors: Array<any> = [];

  constructor() { }

  ngOnInit() {
  }

  pushError(error){

    let existingIdx = this.errors.indexOf(error);
    if(existingIdx > -1){
      return
    }
    this.errors.push({message: `The field No ${this.errors.length + 1} name should be exactly 21 characters`})
  }

  removeErrors(){
    this.errors = [];
  }

  removeError(error){
    this.errors.splice(this.errors.indexOf(error), 1);
  }

}
