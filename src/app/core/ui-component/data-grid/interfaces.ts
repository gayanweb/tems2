/**
 * Created by gayanyapa on 1/4/17.
 */

import {Observable} from 'rxjs/Rx';
import {GridOptions, ICellEditor, SelectCellEditor, ICellEditorParams, IDatasource} from 'ag-grid/main'
import {RequestFilter, Sort} from "../../services/http/filter/interfaces";

/*******************************************************************************
 *                      Paginator
 ******************************************************************************/
export interface Paginator{
  perPage: number,
  page: number
}

/*******************************************************************************
 *                      Datasource
 ******************************************************************************/
export interface DataSource{
  read?(filters?: Array<RequestFilter>, sorts?: Array<Sort>, pagination?:Paginator): Observable<any>,
  create?(data: any): Observable<any>,
  update?(data: any): Observable<any>,
  del?(data: any): Observable<any>
}

export interface Options extends GridOptions{

  title?: string,
  dataSource?: DataSource,
  headerLess?: boolean,
  primaryField?: string,
  filters?: Array<RequestFilter>
}

/*******************************************************************************
 *                      Cell Editor                                            *
 ******************************************************************************/
export interface CellEditor extends ICellEditor{

  // gets called once after the editor is created
  init?(params: ICellEditorParams): void;

  // Gets called once after GUI is attached to DOM.
  // Useful if you want to focus or highlight a component
  // (this is not possible when the element is not attached)
  afterGuiAttached?(): void;

  // Return the DOM element of your editor, this is what the grid puts into the DOM
  getGui(): HTMLElement;

  // Should return the final value to the grid, the result of the editing
  getValue(): any;

  // Gets called once by grid after editing is finished
  // if your editor needs to do any cleanup, do it here
  destroy?(): void;

  // Gets called once after initialised.
  // If you return true, the editor will appear in a popup
  isPopup?(): boolean;

  // Gets called once before editing starts, to give editor a chance to
  // cancel the editing before it even starts.
  isCancelBeforeStart?(): boolean;

  // Gets called once when editing is finished (eg if enter is pressed).
  // If you return true, then the result of the edit will be ignored.
  isCancelAfterEnd?(): boolean;

  // If doing full row edit, then gets called when tabbing into the cell.
  focusIn?(): boolean;

  // If doing full row edit, then gets called when tabbing out of the cell.
  focusOut?(): boolean;
}

/*******************************************************************************
 *                      Dropdown Editor
 ******************************************************************************/
export interface DropDownEditor extends SelectCellEditor{

  // gets called once after the editor is created
  init(params: any): void;

  // Gets called once after GUI is attached to DOM.
  // Useful if you want to focus or highlight a component
  // (this is not possible when the element is not attached)
  afterGuiAttached(): void;

  // Return the DOM element of your editor, this is what the grid puts into the DOM
  getGui(): HTMLElement;

  // Should return the final value to the grid, the result of the editing
  getValue(): any;

  // Gets called once by grid after editing is finished
  // if your editor needs to do any cleanup, do it here
  destroy(): void;

  // Gets called once after initialised.
  // If you return true, the editor will appear in a popup
  isPopup?(): boolean;

  // If doing full row edit, then gets called when tabbing into the cell.
  focusIn(): boolean;
}
