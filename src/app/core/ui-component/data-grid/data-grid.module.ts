import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '@angular/material';
import {MdMenuModule} from '@angular/material/menu';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AgGridModule} from 'ag-grid-ng2/main';
import {DataGridComponent} from './data-grid.component'
import {FormModule} from '../form/form.module'
import {FlexLayoutModule} from '@angular/flex-layout';
import {SelectComponent} from './editors/select/select.component'
import {DatePickerComponent} from './editors/date-picker/date.component'
import {RowHeaderComponent} from './cell-components/row-header/row-header.component'
import {FormsModule}   from '@angular/forms';
import {ErrorContainerComponent} from './validation/error-container/error-container.component';
import {OverlayPanelModule} from 'primeng/primeng';
import {Combo} from "./editors/combo/combo";
import { CellOptionComponent } from './cell-components/cell-option/cell-option.component';
import {FilterModule} from "../filter/filter.module";

@NgModule({
  imports        : [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    MdMenuModule,
    FormsModule,
    NgbModule.forRoot(),
    AgGridModule.withComponents(['enableColResize']),
    OverlayPanelModule,
    FormModule,
    FilterModule
  ],
  declarations   : [
    DataGridComponent,
    SelectComponent,
    DatePickerComponent,
    RowHeaderComponent,
    ErrorContainerComponent,
    Combo,
    CellOptionComponent
  ],
  entryComponents: [
    SelectComponent,
    DatePickerComponent,
    RowHeaderComponent,
    Combo,
    CellOptionComponent
  ],
  exports        : [
    DataGridComponent,
    SelectComponent,
    DatePickerComponent,
    RowHeaderComponent,
    Combo,
    CellOptionComponent
  ],
  bootstrap      : [DataGridComponent]
})
export class DataGridModule {
}
