import { Component, OnInit } from '@angular/core';
import {AgRendererComponent} from "ag-grid-ng2";
import {GridApi} from "ag-grid";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-cell-option',
  templateUrl: './cell-option.component.html',
  styleUrls: ['./cell-option.component.scss']
})
export class CellOptionComponent implements OnInit, AgRendererComponent {

  private params: any;
  private gridApi: GridApi;

  constructor() { }

  agInit(params: any): void {
    this.params = params;
    this.gridApi = params.api;
  }

  ngOnInit() {
  }

  removeRow(){
    let primaryKey = this.params.node.gridOptionsWrapper.gridOptions.primaryField;
    if(!isNullOrUndefined(this.params.data[primaryKey])){
      this.params.data['__REMOVED__'] = true;
      return;
    }
    this.gridApi.removeItems([this.params.node]);
    this.gridApi.refreshView();
  }

  duplicate(){
    let data = Object.assign({}, this.params.data);
    let primaryKey = this.params.node.gridOptionsWrapper.gridOptions.primaryField;
    if(!isNullOrUndefined(data[primaryKey])){
      delete data[primaryKey];
    }

    this.gridApi.addItems([data]);
    this.gridApi.refreshView();
    //get last inserted id
    let lastIndex = this.params.api.getModel().getRowCount() - 1;
    //focus last inserted row
    this.params.api.setFocusedCell(lastIndex, 'name');
    //scroll to focused cell
    this.params.api.ensureIndexVisible(lastIndex);
  }

}
