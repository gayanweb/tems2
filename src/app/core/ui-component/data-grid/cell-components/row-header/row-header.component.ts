import {
  Component, OnInit, ElementRef, AfterViewInit, Renderer, trigger, state, transition, style, animate
} from '@angular/core';
import { MdCheckboxChange } from '@angular/material/checkbox';
import {AgRendererComponent} from "ag-grid-ng2/main";
import {} from 'ag-grid/main'
import {isUndefined} from "util";
import {ErrorResponse} from "../../../../services/http/interfaces";

@Component({
  selector: 'grid-row-header',
  templateUrl: './row-header.component.html',
  styleUrls: ['./row-header.component.scss'],
  animations: [
    /*trigger('animateMessage', [
      /!*state('*', style({
       transform: 'scale(1)'
       })),
       state('*',   style({
       transform: 'scale(1.1)'
       })),*!/
      state('void', style({width: '0', opacity: 0})),
      state('*', style({width: '100px', opacity: 1, transform: 'translateX(0)'})),
      transition('void => *', [
        style({transform: 'translateX(-100%)'}),
        animate('4s ease-in')
      ]),
      transition('* => void', [
        animate('0.3s 10 ease-out', style({transform: 'translateX(100%)', opacity: 0}))

      ])
    ])*/
  ]
})
export class RowHeaderComponent implements AgRendererComponent, OnInit, AfterViewInit {

  private params: any;

  public user: any = {};

  public errors: any;

  public constructor(private element: ElementRef, private renderer: Renderer){}

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.element.nativeElement.focus();
    this.setupStyles();
  }

  agInit(params: any): void {
    this.params = params;
  }

  setupStyles(){
    let width = this.params.column.actualWidth;
    let height = this.params.eGridCell.offsetHeight;
    this.renderer.setElementStyle(this.element.nativeElement.children[0], 'width', `${width}px`);
    this.renderer.setElementStyle(this.element.nativeElement.children[0], 'height', `${height}px`);
  }

  public get options(){
    return this.params.options;
  }

  click(event: Event){
    event.stopPropagation();
  }

  toggleSelectRow(event: MdCheckboxChange){
    this.params.node.setSelected(event.checked);
  }

}
