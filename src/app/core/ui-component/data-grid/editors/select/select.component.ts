import {
  Component, OnInit, ElementRef, AfterViewInit, Renderer, AfterContentInit, EventEmitter,
  Output, ViewChild, AfterViewChecked
} from '@angular/core';
import {MdSelect} from '@angular/material'
import {AgEditorComponent} from "ag-grid-ng2/main";
import {isUndefined} from "util";
import {ErrorResponse} from "../../../../services/http/interfaces";

@Component({
  selector: 'grid-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements AgEditorComponent, OnInit, AfterViewInit, AfterContentInit {

  private params: any;

  public selectedValue: any = undefined;

  @ViewChild('select')
  public selectElement: MdSelect;

  public constructor(private element: ElementRef, private renderer: Renderer){}

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.element.nativeElement.focus();
    this.setupStyles();

    /* @TODO This is dirty way of doing this Please find a proper lifecycle hook */
    setTimeout(_=>this.selectElement.open());
  }

  ngAfterContentInit(){}

  agInit(params: any): void {
    this.params = params;
    this.params.api.addEventListener(`inputError`, (error: ErrorResponse) => {
      // console.log(error);
    });
  }

  getValue(): any {
    return !isUndefined(this.selectedValue) ? this.selectedValue : this.params.value;
  }

  setupStyles(){
    let width = this.params.column.actualWidth;
    let height = this.params.eGridCell.offsetHeight;
    this.renderer.setElementStyle(this.element.nativeElement.children[0], 'width', `${width}px`);
    this.renderer.setElementStyle(this.element.nativeElement.children[0], 'height', `${height}px`);
  }

  isPopup(): boolean {
    return false;
  }

  public get options(){
    return this.params.options;
  }

  afterGuiAttached(){
    alert(234234);
  }

  onSelect(){
    this.params.stopEditing();
  }

  onKeyDown(event): void {
    let key = event.which || event.keyCode;
    if (key == 37 ||  // left
      key == 39) {  // right
      event.stopPropagation();
    }
  }

}
