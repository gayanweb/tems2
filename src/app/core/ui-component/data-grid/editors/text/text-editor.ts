/**
 * Created by gayanyapa on 1/30/17.
 */
import {CellEditor} from '../../interfaces'
import {ICellEditorParams} from 'ag-grid/main'

export class TextEditor implements CellEditor {

  private eInput: HTMLInputElement;

  init(params: ICellEditorParams): void {
    this.eInput = document.createElement('input');
    this.eInput.value = params.value;
  }

  getGui(): HTMLElement {
    return this.eInput;
  }

  getValue(): any {
    return this.eInput.value;
  }

  // focus and select can be done after the gui is attached
  afterGuiAttached() {
    this.eInput.focus();
    this.eInput.select();
  };

// any cleanup we need to be done here
  destroy() {
    // but this example is simple, no cleanup, we could
    // even leave this method out as it's optional
  };

// if true, then this editor will appear in a popup
  isPopup() {
    return false;
  };

}
