import { Component, OnInit, ElementRef, AfterViewInit, Renderer, AfterContentInit, ViewChild } from '@angular/core';
import {AgEditorComponent} from "ag-grid-ng2/main";
import {NgbDateStruct, NgbInputDatepicker} from "@ng-bootstrap/ng-bootstrap";
import {isUndefined} from "util";

@Component({
  selector: 'grid-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.css']
})
export class DatePickerComponent implements AgEditorComponent, OnInit, AfterViewInit, AfterContentInit {

  private params: any;

  public user: any = {};

  private selectedDate: NgbDateStruct;

  @ViewChild('date')
  private dateElement: NgbInputDatepicker;

  public constructor(private element: ElementRef, private renderer: Renderer){}

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.element.nativeElement.focus();
    this.setupStyles();

    /* @TODO This is dirty way of doing this Please find a proper lifecycle hook */
    setTimeout(_ => {
      let now = new Date();
      this.selectedDate = {year: now.getFullYear(), month: now.getMonth(), day: now.getDate()};
      this.dateElement.open();
      this.dateElement.navigateTo({
        year: now.getFullYear(), month: now.getMonth()
      });
    });

  }

  agInit(params: any): void {
    this.params = params;
  }

  ngAfterContentInit(){

  }

  getValue(): any {

    if(isUndefined(this.selectedDate)){
      return this.params.value;
    }
    let date = this.selectedDate;
    let dt = new Date();
    dt.setDate(date.day);
    dt.setMonth(date.month);
    dt.setFullYear(date.year);

    return dt;
  }

  setupStyles(){
    let width = this.params.column.actualWidth;
    let height = this.params.eGridCell.offsetHeight;
    this.renderer.setElementStyle(this.element.nativeElement.children[0], 'width', `${width}px`);
    this.renderer.setElementStyle(this.element.nativeElement.children[0], 'height', `${height}px`);
  }

  isPopup(): boolean {
    return true;
  }

  public get options(){
    return this.params.options;
  }

  afterGuiAttached(){
    alert(234234);
  }

  onSelect(){
    this.params.api.stopEditing();
  }

  onClick(happy: boolean) {
    this.params.api.stopEditing();
  }

  onKeyDown(event): void {
    console.log(2342342);
    let key = event.which || event.keyCode;
    if (key == 37 ||  // left
      key == 39) {  // right
      event.stopPropagation();
    }
  }

}
