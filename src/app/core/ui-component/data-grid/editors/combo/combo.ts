import {InputComboComponent} from "../../../input-combo/input-combo.component";
import {OnInit, OnDestroy, AfterViewInit, Component} from "@angular/core";
import {AgEditorComponent} from "ag-grid-ng2";
import {isNullOrUndefined} from "util";
import {MdDialog, MdDialogConfig} from "@angular/material";
import {PopupComponent} from "../../../input-combo/popup/popup.component";
import {Observable} from "rxjs";
/**
 * Created by gayanyapa on 2/17/17.
 */
@Component({
  selector   : 'grid-combo',
  templateUrl: '../../../input-combo/input-combo.component.html',
//  styleUrls: ['../../../input-combo/input-combo.component.scss']
})
export class Combo extends InputComboComponent implements OnInit, OnDestroy, AgEditorComponent, AfterViewInit {

  private params: any = undefined;

  constructor(dialog: MdDialog) {
    super(dialog)
  }

  agInit(params: any): void {
    this.params = params;
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    let observable = new Observable(() => {
      setTimeout(_ => this.openPopup());
    });
    this.subscriptions.push(observable.subscribe());
  }

  getValue(): any {
    return isNullOrUndefined(this.selectedObject) ? this.params.value : this.selectedObject;
  }

  openPopup() {
    let config = new MdDialogConfig();
    config.height = '70%';
    config.width = '70%';

    let dialogRef = this.dialog.open(PopupComponent, config);
    this.subscriptions.push(dialogRef.afterClosed().subscribe(data => {
      this.selectedObject = data;
      this.params.stopEditing();
    }));
  }
}
