import {
  Component, OnInit, OnDestroy, AfterViewInit, ElementRef,
  Renderer, Input, AfterContentInit
} from '@angular/core';


import {DataSource, Options} from './interfaces'
import {RowNode, StartEditingCellParams} from 'ag-grid/main'

import {Response} from '@angular/http';
import {Observable, Subscription} from 'rxjs/Rx';

import {RowHeaderComponent} from './cell-components/row-header/row-header.component'
import {isNullOrUndefined} from "util";
import {RequestFilter} from "../../services/http/filter/interfaces";
import {Field, FieldType, Filter} from "../filter/interfaces";
import {ErrorResponse, HTTP_STATUS} from "../../services/http/interfaces";

@Component({
  selector: 'data-grid',
  templateUrl: './data-grid.component.html',
  styleUrls: ['./data-grid.component.scss']
})
export class DataGridComponent implements OnInit, OnDestroy, AfterViewInit, AfterContentInit {

  @Input() public dataSource: DataSource;
  @Input() public rowData: any = undefined;
  @Input() public gridOptions: Options = undefined;

  public selectedRowCount: number = 0;

  public page: number = 2;

  public loadPanel: boolean = true;

  private changedData: Array<RowNode> = [];

  private subscriptions: Array<Subscription> = [];

  private filters: Array<Filter> = [];

  private validationErrors = [];

  private defautFIlters: Array<RequestFilter> = [{
    field: 'asdasd',
    operator: 2,
    value: 2
  }];

  private filterFields: Array<Field> = [
    {name: 'name', visibleName: 'Name', type: FieldType.TEXT},
    {name: 'id', visibleName: 'Id', type: FieldType.NUMBER},
    {name: 'date', visibleName: 'Date', type: FieldType.DATE},
  ];


  constructor(private elementDef: ElementRef, public re: Renderer) {
  }

  pushError(){
    this.validationErrors.push({message: `The field No ${this.validationErrors.length + 1} name should be exactly 21 characters`})
  }

  removeErrors(){
    this.validationErrors = [];
  }

  removeError(error){
    this.validationErrors.splice(this.validationErrors.indexOf(error), 1);
  }

  ngOnInit() {

    this.gridOptions.suppressLoadingOverlay = true;
    this.gridOptions.rowSelection = 'multiple';
    this.gridOptions.primaryField = isNullOrUndefined(this.gridOptions.primaryField) ? 'id' : this.gridOptions.primaryField;
    this.gridOptions.suppressRowClickSelection = true;
    this.gridOptions.animateRows = true;
    this.gridOptions.headerLess = isNullOrUndefined(this.gridOptions.headerLess) ? false : this.gridOptions.headerLess;
    this.gridOptions.onGridReady = () => {
      this.gridOptions.api.sizeColumnsToFit();
    };

    this.gridOptions.onSelectionChanged = () => {
      this.selectedRowCount = this.gridOptions.api.getSelectedNodes().length;
    };

    this.gridOptions.onCellValueChanged = (event) => {
      console.log(event);
      if (this.changedData.indexOf(event.node) > -1 || event.oldValue === event.newValue) {
        return;
      }
      this.changedData.push(event.node);
    };

    this.gridOptions.columnDefs.unshift({
      headerName: '#',
      // checkboxSelection: true,
      field: 'id',
      suppressMenu: true,
      // pinned: 'left',
      cellRendererFramework: RowHeaderComponent,
      width: 60,
      suppressSorting: true,
    });

    /*this.gridOptions.defaultColDef = {
     cellRendererFramework: RowHeaderComponent
     };*/

    this.initOptions();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  ngAfterViewInit() {}

  ngAfterContentInit() {
    this.dataSource.read().subscribe(data => {
      this.gridOptions.rowData = data;
    });
  }

  setupElement() {
    let element = this.elementDef.nativeElement.childNodes[4];
    console.log(this.elementDef.nativeElement.childNodes[2].childNodes);
    let height = element.clientHeight;
    this.re.setElementStyle(element, 'height', `${height - 48}px`);
  }

  initOptions() {
    if (this.dataSource) {
      this.loadData();
    }
  }

  loadData() {
    this.loading();
    this.subscriptions.push(this.dataSource.read().finally(() => {
      this.loaded();
    }).subscribe(data => {
      this.rowData = data;
    }));
  }

  loading() {
    this.loadPanel = true;
  }

  loaded() {
    this.loadPanel = false;
  }

  insertRow() {
    //add an empty object
    this.gridOptions.api.addItems([{}]);
    //get last inserted id
    let lastIndex = this.gridOptions.api.getModel().getRowCount() - 1;
    //focus last inserted row
    this.gridOptions.api.setFocusedCell(lastIndex, 'name');
    //scroll to focused cell
    this.gridOptions.api.ensureIndexVisible(lastIndex);
    //start editing focused cell
    this.gridOptions.api.startEditingCell({
      rowIndex: lastIndex, colKey: 'name'
    });
  }

  saveChangedRows() {
    let observables: Array<Observable<any>> = [];

    this.loading();
    let rowsToSave = this.changedData.length;

    this.changedData.forEach((node: RowNode, index) => {
      let observable: Observable<any>;
      if (isNullOrUndefined(node.data['id'])) {
        observable = this.dataSource.create(node.data);
      } else {
        observable = this.dataSource.update(node.data);
      }

      this.subscriptions.push(observable.finally(() => {
        rowsToSave--;
        if (rowsToSave < 1) {
          this.loaded();
        }

      }).subscribe(data => {
        let primaryKey = this.gridOptions.primaryField;
        let idx = this.changedData.findIndex((value: RowNode) => node.data[primaryKey] === data[primaryKey]);
        this.changedData.splice(idx, 1);
      }, (error: ErrorResponse) => {
        if(error.getStatus() === HTTP_STATUS.HTTP_UNPROCESSABLE_ENTITY){
          for (let message of error.response()){
            let messages = message.message;
            for(let msg in messages){
              this.validationErrors.push({
                message: `Invalid data for #${error.getRef()} => ${messages[msg]}`
              });

              let params: StartEditingCellParams = {
                rowIndex: 8,
                colKey: 'address'
              };

              this.gridOptions.api.startEditingCell(params);
              this.gridOptions.api.dispatchEvent(`inputError`, error);


              console.log(msg, messages[msg]);
            }
          }
          /*error.response().forEach((error) => {
            console.log(error);
          })*/
        }
        this.gridOptions.api.dispatchEvent(`inputError${node.id}`, error);
        console.log(error.getRef());
      }));

    });

    //run all
    /*this.loading();
     this.subscriptions.push(Observable.forkJoin(observables).finally(() => {
     this.loaded();
     }).subscribe(data => {
     console.log(data);
     // let idx = this.changedData.findIndex((value: RowNode) => node.data['id'] === data['id']);
     // this.changedData.splice(idx, 1);
     }, (error: ErrorResponse) => {
     console.log(error.response());
     // this.gridOptions.api.dispatchEvent('inputError', error);
     }));

     this.dataSource.create(this.changedData);*/
  }

  deleteSelectedRows() {
    let rows = this.gridOptions.api.getSelectedNodes();
    let observables: Array<Observable<any>> = [];
    rows.forEach((node, index) => {
      observables.push(this.dataSource.del(node.data).map(() => {
        return {id: node.data['id']}
      }).catch(e => {
        console.log(e);
        return Observable.throw(e);
      }));
    });

    this.loading();
    this.subscriptions.push(Observable.forkJoin(observables).finally(() => {
      this.loaded();
    }).catch((error: Response| any) => {
      console.log(error);
      return Observable.throw(error);
    }).subscribe((data) => {
      console.log(data);
      /*data.forEach((item) => {
       console.log(item);
       /!*this.gridOptions.rowData.findIndex((node: RowNode) => {
       return node.data.id == item.id
       })*!/
       });*/
      // this.gridOptions.api.removeItems([node]);
      // this.removeFromChangedData(node)
    }));
  }

  removeFromChangedData(node: RowNode) {
    if (this.changedData.indexOf(node) > -1) {
      this.changedData.splice(this.changedData.indexOf(node), 1);
    }
  }

  read() {

  }

  insert() {

  }

  update() {

  }

  del() {

  }

  applyFilter(filter: Filter) {
    let existingIdx = this.filters.findIndex(object => {
      return JSON.stringify(object) === JSON.stringify(filter);
    });
    if (existingIdx > -1) {
      this.filters.splice(existingIdx ,1);
    }

    this.filters.push(filter);
  }

  removeFilter(filter: Filter) {
    this.filters.splice(this.filters.indexOf(filter), 1);
  }


  // cellClicked(event){
  //   console.log(event);
  // }


  /*loadData() {
   console.log('loading....');
   return this.http.get('https://jsonplaceholder.typicode.com/posts', {
   filters: [
   {field: 'name', value: 'some', operator: 2}
   ]
   }).retryWhen(error => {
   return error.flatMap(e => {

   return Observable.throw('error');
   });

   }).map(
   (response: Response) => response.json()
   ).catch((error: Response | any) => {
   console.log(Observable);
   return Observable.throw('error');
   }).finally(() => {
   console.log('loaded');
   }).subscribe(data => {
   this.data = data;
   });
   }*/


}
