/**
 * Created by gayanyapa on 2/3/17.
 */

import {RequestFilter as ApiFilter} from "../../services/http/filter/interfaces";
export enum FieldType{
  TEXT = 1,
  NUMBER = 2,
  DATE = 3,
}

export interface Field{
  name: string,
  visibleName?: string,
  type: FieldType
}

export interface Filter{
  filter: ApiFilter,
  filterType: FilterDef,
  field: Field
}

export interface FilterDef{
  name: string,
  value: FilterType
}

export enum FilterType{
  EQUAL = 1,
  CONTAINS = 2,
  LESS_THAN = 3,
  GREATER_THAN = 4,
  BETWEEN = 5,
  IN = 6,
  NOT_IN = 7
}
