import { Component, OnInit, Input, Output, EventEmitter, trigger, state, style, transition, animate } from '@angular/core';
import {Filter} from "../../filter/interfaces";

@Component({
  selector: 'app-filter-viewer',
  templateUrl: './filter-viewer.component.html',
  styleUrls: ['./filter-viewer.component.scss'],
  animations: [
    trigger('filter', [
      /*state('*', style({
        transform: 'scale(1)'
      })),
      state('*',   style({
        transform: 'scale(1.1)'
      })),*/
      state('in', style({transform: 'translateX(0)'})),
      transition('void => *', [
        style({transform: 'translateX(+100%)', opacity: 0}),
        animate('0.3s ease-in')
      ]),
      transition('* => void', [
        animate('0.3s 10 ease-out', style({transform: 'translateX(100%)', opacity: 0}))

      ])
    ])
  ]
})
export class FilterViewerComponent implements OnInit {

  @Input()
  public filters: Array<Filter> = [];

  @Output()
  public onRemove: EventEmitter<Filter> = new EventEmitter<Filter>();

  constructor() { }

  ngOnInit() {
  }

  remove(filter: Filter){
    this.onRemove.emit(filter);
  }

}
