import {Component, OnInit, Output, Input, EventEmitter, AfterViewInit, AfterContentInit} from '@angular/core';
import {FilterType, FieldType, Field, Filter, FilterDef} from "./interfaces";
import {isUndefined} from "util";
import {RequestFilter} from "../../services/http/filter/interfaces";

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit, AfterViewInit, AfterContentInit {

  @Input()
  public filters: RequestFilter;

  @Input()
  public fields: Array<Field>;

  @Output()
  public onSelect: EventEmitter<Filter> = new EventEmitter<Filter>();

  @Output()
  public onChange: EventEmitter<Filter> = new EventEmitter<Filter>();

  private selectedField: Field;

  private filterDefs: Array<FilterDef> = [
    {name: 'Equal', value: FilterType.EQUAL},
    {name: 'Contains', value: FilterType.CONTAINS},
    {name: 'Between', value: FilterType.BETWEEN},
    {name: 'In', value: FilterType.IN}
  ];

  private appliedFilters: Array<Filter> = [];

  private filterType = FilterType;

  private selectedFilters: Filter = null;

  private selectedFilter: any = this.filterDefs[0];

  constructor() {}

  ngOnInit() {}

  ngAfterViewInit(){}

  ngAfterContentInit(){
    if(this.fields.length > 0){
     this.selectedField = this.fields[0];
     }
  }

  selectField(field: Field) {
    this.selectedField = field;
  }

  selectFilter(filter: FilterDef) {
    this.selectedFilter = filter;
  }

  getTypeDef(field: Field) {
    if(isUndefined(field)){
      return 'text';
    }

    switch (field.type) {
      case FieldType.NUMBER:
        return 'number';

      case FieldType.DATE:
        return 'date';

      default :
        return 'text';
    }
  }

  renderFilter(value) {
    if(isUndefined(this.selectedField)){
      return;
    }
    let filterVal: RequestFilter = {
      field: this.selectedField.name,
      operator: this.selectedFilter.value,
      value: value
    };

    let filter: Filter = {
      filter:filterVal,
      field: this.selectedField,
      filterType: this.selectedFilter
    };

    this.applyFilter(filter);

    this.onSelect.emit(filter)
  }

  onTextChange(value){ console.log(value);
    this.onSelect.emit(value);
  }

  apply(event){
     if(event.key == 'Enter'){
//      this.renderFilter();
     }
    // console.log(event.key);
  }

  appendMenu(e: Event){
    e.stopPropagation();
  }

  xxx(){
    alert(234234);
  }

  applyFilter(filter: Filter) {
    let existingIdx = this.appliedFilters.findIndex(object => {
      return JSON.stringify(object) === JSON.stringify(filter);
    });
    if (existingIdx > -1) {
      this.appliedFilters.splice(existingIdx ,1);
    }

    this.appliedFilters.push(filter);
  }

  removeFilter(filter: Filter) {
    this.appliedFilters.splice(this.appliedFilters.indexOf(filter), 1);
  }

}
