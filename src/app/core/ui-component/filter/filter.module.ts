import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MaterialModule } from '@angular/material'
import { FormModule } from '../form/form.module';
import { Http } from "../../services/http/http";
import { FilterComponent } from "./filter.component";
import { InputComponent } from "./fields/input/input.component";
import { DateComponent } from "./fields/date/date.component";
import { FilterViewerComponent } from "./filter-viewer/filter-viewer.component";

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    NgbModule,
    MaterialModule,
    FormModule
  ],
  declarations: [FilterComponent, FilterViewerComponent, InputComponent, DateComponent],
  entryComponents: [FilterComponent, FilterViewerComponent],
  exports: [FilterComponent, FilterViewerComponent],
  providers: [Http],
})
export class FilterModule { }
