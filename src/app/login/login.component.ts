import { Component, OnInit } from '@angular/core';
import {LoginService} from "./login.service";
import {User} from "./interfaces";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private _loading: boolean = false;

  constructor(private loginService: LoginService) { }

  ngOnInit() {
  }

  loading(){
    this._loading = true;
  }

  loaded(){
    this._loading = false;
  }

  login(){
    this.loading();
    this.loginService.authenticate().finally(() => this.loaded()).subscribe((user: User) => {
      localStorage.setItem('user_id', `${user.id}`);
    });
  }



}
