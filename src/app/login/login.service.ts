import { Injectable } from '@angular/core';
import {Http} from "../core/services/http/http";
import {Observable, Subject} from "rxjs";
import {User} from "./interfaces";

@Injectable()
export class LoginService {

  private _token = new Subject();

  private user: User = {
    id: 1,
    name: 'Gayan Yapa'
  };

  constructor(private http: Http) {}

  get token(){
    return this._token;
  }

  setToken(token: string){
    this._token.next(token);
    localStorage.setItem('token', token);
  }

  authenticate(): Observable<User>{
    return Observable.timer(2000).map(_ => {
      this.setToken(`${this.user.id}`);
      return this.user;
    });
  }

  invalidate(){
    this._token.next(null);
    localStorage.removeItem('token');
  }

}
