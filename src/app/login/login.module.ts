import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from "./login.component";
import { MaterialModule } from "@angular/material";
import { FlexLayoutModule } from "@angular/flex-layout";
import {Http} from "../core/services/http/http";
import {FormsModule} from "@angular/forms";
import {FormModule} from "../core/ui-component/form/form.module";
import {LoginService} from "./login.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    FlexLayoutModule,
    FormModule
  ],
  declarations: [
    LoginComponent
  ],
  providers: [Http, LoginService]
})
export class LoginModule {
}
