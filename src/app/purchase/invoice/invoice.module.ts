import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceComponent } from './invoice/invoice.component';
import {DataGridModule} from "../../core/ui-component/data-grid/data-grid.module";
import {MaterialModule} from "@angular/material";
import {FlexLayoutModule} from "@angular/flex-layout";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {InputComboModule} from "../../core/ui-component/input-combo/input-combo.module";
import {BrokerModule} from "../../setup/broker/broker.module";

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    MaterialModule,
    FlexLayoutModule,
    NgbModule,
    InputComboModule,
    DataGridModule,
    BrokerModule
  ],
  declarations: [InvoiceComponent]
})
export class InvoiceModule { }
