import { Component, OnInit } from '@angular/core';
import {Subscription, Observable} from "rxjs";
import {Options, DataSource} from "../../../core/ui-component/data-grid/interfaces";
import {DatePickerComponent} from "../../../core/ui-component/data-grid/editors/date-picker/date.component";
import {SelectComponent} from "../../../core/ui-component/data-grid/editors/select/select.component";
import {Combo} from "../../../core/ui-component/data-grid/editors/combo/combo";
import {Response} from "@angular/http";
import {ErrorResponse} from "../../../core/services/http/error-response";
import {Http} from "../../../core/services/http/http";
import {CellOptionComponent} from "../../../core/ui-component/data-grid/cell-components/cell-option/cell-option.component";
import {ComboOptions} from "../../../core/ui-component/input-combo/interfaces";
import {FieldType} from "../../../core/ui-component/filter/interfaces";
import {BrokerComponent} from "../../../setup/broker/broker.component";

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {

  protected subscriptions: Array<Subscription> = [];

  private comboOptions: ComboOptions;

  public gridOptions: Options = {
    rowHeight: 40,
    headerLess: true
  };

  private invoiceSummery: any = {
    total: 39000,
    discount: 56
  };

  public dataSource: DataSource;

  constructor(private http: Http) { }

  ngOnInit() {
    this.gridOptions.columnDefs = [
      {headerName: '#', field: 'id', suppressMenu: true, suppressSorting: true},
      {headerName: 'Name', field: 'name', cellEditorFramework: DatePickerComponent, editable: true},
      {
        headerName         : 'Address', cellEditorParams: {
        options: [
          {name: 'Val 111', value: 111},
          {name: 'Val 222', value: 222},
          {name: 'Val 333', value: 333}
        ]
      },
        cellEditorFramework: SelectComponent,
        field              : 'address', editable: true
      },
      {headerName: 'Tel', cellEditor: 'select', editable: true, field: 'telephone'},
      {headerName: 'Code', field: 'code', cellEditorFramework: Combo, editable: true},
      {headerName: 'Free Storage', field: 'freeStorage'},
      {headerName: '', field: 'options', cellRendererFramework: CellOptionComponent, width: 60},
    ];

    this.dataSource = {
      read  : () => {
        return this.loadData()
      },
      create: (data) => {
        return this.saveData(data)
      },
      update: (data) => {
        return this.updateData(data.id, data);
      },
      del   : (data) => {
        return this.deleteData(data.id);
      }
    };

    this.comboOptions = {
      fields: [{name: 'id', field: 'Id'}],
      keyParam: 'id',
      valParam: 'name',
      onSelect: selected => {
        console.log(selected);
      },
      popupOptions:{
        dataSource: {
          read: (filters) => {
            return this.loadData(filters)
          }
        },
        newComponentRef: BrokerComponent,
        filters: [
          {name: 'name', visibleName: 'Name', type: FieldType.TEXT},
          {name: 'id', visibleName: 'Id', type: FieldType.NUMBER},
          {name: 'date', visibleName: 'Date', type: FieldType.DATE},
        ]
      }
    }


  }

  loadData(filters = []): Observable<any> {
    console.log('loading....');
    return this.http.get('http://api.tems.local/api/v1/broker', {
      filters: filters
    }).retryWhen(error => {
      return error.flatMap(e => {

        return Observable.throw('error');
      });

    }).map(
      (response: Response) => {console.log(response.json().data); return response.json().data}
    ).catch((error: Response | any) => {
      console.log(Observable);
      return Observable.throw('error');
    });
  }

  saveData(data): Observable<any> {
    return this.http.post('http://api.tems.local/api/v1/broker', data)
      .retryWhen(error => error.flatMap(e => Observable.throw('error')))
      .map((response: Response) => response.json())
      .catch((error: Response | any) => Observable.create(err => error, err => error));
  }

  updateData(id, data): Observable<any> {
    console.log('loading....');
    return this.http.put(`http://api.tems.local/api/v1/broker/${id}`, data)
    /*.retryWhen(error => {
     return error.flatMap(e => {

     return e;
     });

     })*/
      .map(
        (response: Response) => id
      ).catch((error: Response | any) => {
        let err = new ErrorResponse(error);
        this.log(err.response());
        // console.log(err.response());
        err.setRef(id);
        return Observable.throw(err);
      });
  }

  log(lg) {
    console.log(lg);
  }

  deleteData(id): Observable<any> {
    console.log('loading....');
    return this.http.delete(`https://jsonplaceholder.typicode.com/posts/${id}`).retryWhen(error => {
      return error.flatMap(e => {
        return Observable.throw({ref: id, error: e});
      });
    }).map(
      (response: Response) => id
    ).catch((error: Response | any) => {
      console.log(error);
      return Observable.throw(new ErrorResponse(error));
    });
  }

}
