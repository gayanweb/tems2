/**
 * Created by gayanyapa on 2/13/17.
 */
import { animate, AnimationEntryMetadata, state, style, transition, trigger } from '@angular/core';

// Component transition animations
export const slideInDownAnimation: AnimationEntryMetadata =
  trigger('routeAnimation', [
    state('*',
      style({
        opacity: 1,
        // transform: 'translateX(0)'
      })
    ),
    transition(':enter', [
      style({
        opacity: 0,
        // transform: 'translateX(-100%)'
      }),
      animate('0.2s')
    ]),
    transition(':leave', [
      animate('0.5s', style({
        opacity: 0,
        // transform: 'translateY(100%)'
      }))
    ])
  ]);
