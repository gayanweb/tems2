import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrokerComponent } from './broker.component'
import { BrokerService } from './broker.service'
import { RouterModule, Routes } from '@angular/router';

import { DataGridModule } from '../../core/ui-component/data-grid/data-grid.module'

/*const appRoutes : Routes = [
  {path: 'main/broker', component: BrokerComponent}
  // {path: 'broker', component: BrokerComponent},
];*/

@NgModule({
  imports: [
    CommonModule,
    DataGridModule
    // RouterModule.forRoot(appRoutes),
  ],
  declarations: [

    BrokerComponent
  ],
  providers: [BrokerService],
  exports: [BrokerComponent]
})
export class BrokerModule { }
