import {Component, OnInit} from '@angular/core';
import {Http} from '../../core/services/http/http';
import {DataSource} from '../../core/ui-component/data-grid/interfaces';
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {Options} from '../../core/ui-component/data-grid/interfaces'


import {TextEditor} from '../../core/ui-component/data-grid/editors/text/text-editor'
import {SelectComponent} from '../../core/ui-component/data-grid/editors/select/select.component'
import {DatePickerComponent} from '../../core/ui-component/data-grid/editors/date-picker/date.component'


import {NotificationService} from '../../core/services/notification/notification.service'
import {Data} from "@angular/router";
import {MdDialogConfig} from "@angular/material";
import {MdDialog} from "@angular/material";
import {ErrorResponse} from "../../core/services/http/error-response";
import {InputComboComponent} from "../../core/ui-component/input-combo/input-combo.component";
import {Combo} from "../../core/ui-component/data-grid/editors/combo/combo";

@Component({
  selector   : 'app-broker',
  templateUrl: './broker.component.html',
  styleUrls  : ['./broker.component.scss'],
  providers  : [Http]
})
export class BrokerComponent implements OnInit {

  public gridOptions: Options = {
    rowHeight: 40
  };

  public dataSource: DataSource;

  constructor(private http: Http, private dialog: MdDialog) {
  }

  ngOnInit() {
    this.gridOptions.columnDefs = [
      {headerName: '#', field: 'id', suppressMenu: true, suppressSorting: true},
      {headerName: 'Name', field: 'name', cellEditorFramework: DatePickerComponent, editable: true},
      {
        headerName         : 'Address', cellEditorParams: {
        options: [
          {name: 'Val 111', value: 111},
          {name: 'Val 222', value: 222},
          {name: 'Val 333', value: 333}
        ]
      },
        cellEditorFramework: SelectComponent,
        field              : 'address', editable: true
      },
      {headerName: 'Tel', cellEditor: 'select', editable: true, field: 'telephone'},
      {headerName: 'Code', field: 'code', cellEditorFramework: Combo, editable: true},
      {headerName: 'Free Storage', field: 'freeStorage'},
    ];

    this.dataSource = {
      read  : () => {
        return this.loadData()
      },
      create: (data) => {
        return this.saveData(data)
      },
      update: (data) => {
        return this.updateData(data.id, data);
      },
      del   : (data) => {
        return this.deleteData(data.id);
      }
    };


  }

  loadData(): Observable<any> {
    console.log('loading....');
    return this.http.get('http://api.tems.local/api/v1/broker', {
      /*filters: [
       {field: 'name', value: 'some', operator: 2}
       ]*/
    }).retryWhen(error => {
      return error.flatMap(e => {

        return Observable.throw('error');
      });

    }).map(
      (response: Response) => response.json().data
    ).catch((error: Response | any) => {
      console.log(Observable);
      return Observable.throw('error');
    });
  }

  saveData(data): Observable<any> {
    return this.http.post('http://api.tems.local/api/v1/broker', data)
      .retryWhen(error => error.flatMap(e => Observable.throw('error')))
      .map((response: Response) => response.json())
      .catch((error: Response | any) => Observable.create(err => error, err => error));
  }

  updateData(id, data): Observable<any> {
    console.log('loading....');
    return this.http.put(`http://api.tems.local/api/v1/broker/${id}`, data)
    /*.retryWhen(error => {
     return error.flatMap(e => {

     return e;
     });

     })*/
      .map(
        (response: Response) => id
      ).catch((error: Response | any) => {
        let err = new ErrorResponse(error);
        this.log(err.response());
        // console.log(err.response());
        err.setRef(id);
        return Observable.throw(err);
      });
  }

  log(lg) {
    console.log(lg);
  }

  deleteData(id): Observable<any> {
    console.log('loading....');
    return this.http.delete(`https://jsonplaceholder.typicode.com/posts/${id}`).retryWhen(error => {
      return error.flatMap(e => {
        return Observable.throw({ref: id, error: e});
      });
    }).map(
      (response: Response) => id
    ).catch((error: Response | any) => {
      console.log(error);
      return Observable.throw(new ErrorResponse(error));
    });
  }

  openDialog() {


    let config = new MdDialogConfig();
    config.height = '80%';
    config.width = '80%';

    let dialogRef = this.dialog.open(BrokerComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      //this.selectedOption = result;
    })
  }

}
