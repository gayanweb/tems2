import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Http} from "../../core/services/http/http";
import {Response} from "@angular/http";
import {ErrorResponse} from "../../core/services/http/error-response";
import {Broker} from "../../data/interfaces";

@Injectable()
export class BrokerService {

  constructor(private http: Http) { }

  getAll(): Observable<Broker> {
    console.log('loading....');
    return this.http.get('http://api.tems.local/api/v1/broker', {
      /*filters: [
       {field: 'name', value: 'some', operator: 2}
       ]*/
    }).retryWhen(error => {
      return error.flatMap(e => {

        return Observable.throw('error');
      });

    }).map(
      (response: Response) => response.json().data
    ).catch((error: Response | any) => {
      console.log(Observable);
      return Observable.throw('error');
    });
  }

  save(broker: Broker): Observable<Broker> {
    return this.http.post('http://api.tems.local/api/v1/broker', broker)
      .retryWhen(error => error.flatMap(e => Observable.throw('error')))
      .map((response: Response) => response.json())
      .catch((error: Response | any) => Observable.create(err => error, err => error));
  }

  updateData(id, data): Observable<Broker> {
    console.log('loading....');
    return this.http.put(`http://api.tems.local/api/v1/broker/${id}`, data)
    /*.retryWhen(error => {
     return error.flatMap(e => {

     return e;
     });

     })*/
      .map(
        (response: Response) => id
      ).catch((error: Response | any) => {
        let err = new ErrorResponse(error);
        // console.log(err.response());
        err.setRef(id);
        return Observable.throw(err);
      });
  }

  deleteData(id): Observable<Broker> {
    console.log('loading....');
    return this.http.delete(`https://jsonplaceholder.typicode.com/posts/${id}`).retryWhen(error => {
      return error.flatMap(e => {
        return Observable.throw({ref: id, error: e});
      });
    }).map(
      (response: Response) => id
    ).catch((error: Response | any) => {
      console.log(error);
      return Observable.throw(new ErrorResponse(error));
    });
  }

}
