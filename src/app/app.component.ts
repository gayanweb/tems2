import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MdIconRegistry } from '@angular/material/icon/icon-registry'
import {LoginService} from "./login/login.service";
import {isNullOrUndefined} from "util";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  // styles: [`app-root {display: flex; flex-direction: column}`],
  styleUrls: [
    './app.component.css',
    './app.component.theme.scss'
  ]
})
export class AppComponent {
  title = 'app works!';

  constructor(
    iconRegistry: MdIconRegistry,
    snitizer: DomSanitizer,
    private loginService: LoginService,
    private router: Router,
  ){
    // iconRegistry.setDefaultFontSetClass('Material Icons');
    iconRegistry.registerFontClassAlias('material', 'material-icons');
    iconRegistry.registerFontClassAlias('font-awesome', 'fa');

    this.loginService.token.subscribe(token => {
      if(isNullOrUndefined(token)){
        this.router.navigate(['login']);
        return;
      }
      this.router.navigate(['main']);
    });
  }

}
