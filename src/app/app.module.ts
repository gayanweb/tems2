import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, ConnectionBackend, XHRBackend } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

/* 3rd party dependencies */
import { MaterialModule } from '@angular/material';
import { MdToolbarModule } from '@angular/material/toolbar'
import { MdProgressSpinnerModule } from '@angular/material/progress-spinner'
import { FlexLayoutModule } from '@angular/flex-layout'
import { MdIconRegistry } from '@angular/material/icon/icon-registry'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';


import { NotificationModule } from './core/services/notification/notification.module'
import { InputComboModule } from './core/ui-component/input-combo/input-combo.module'

import { AgGridModule } from 'ag-grid-ng2/main';

import { DataTableModule ,SharedModule,EditorModule } from 'primeng/primeng';
import { FormModule } from './core/ui-component/form/form.module';
import { DataGridModule } from './core/ui-component/data-grid/data-grid.module';
import { AppRoutingModule } from "./app.routes";
import { MainModule } from "./main/main.module";
import { LoginModule } from "./login/login.module";
import { InvoiceModule } from "./purchase/invoice/invoice.module";
import {ToolbarModule} from "./core/ui-component/toolbar/toolbar.module";


const appRoutes : Routes = [
  {path: 'login', component: LoginComponent},
  // {path: 'broker', component: BrokerComponent},
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    AgGridModule.withComponents(['enableColResize']),
    HttpModule,
    FormsModule,
    DataTableModule,SharedModule, EditorModule,
    AppRoutingModule,
    MaterialModule,
    MdToolbarModule,
    MdProgressSpinnerModule,
    FlexLayoutModule,
    NotificationModule,
    FormModule,
    InputComboModule,
    DataGridModule,
    ToolbarModule,
    MainModule,
    LoginModule,
    InvoiceModule
  ],

  providers: [MdIconRegistry, {provide: ConnectionBackend, useClass: XHRBackend}],
  bootstrap: [AppComponent]
})
export class AppModule { }
