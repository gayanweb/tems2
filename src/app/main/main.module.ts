import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from "./main.component";
import { LogoutComponent } from '../logout/logout.component';
import { GridDirective } from '../core/ui-component/grid.directive';
import { ProfileComponent } from '../profile/profile.component';
import { BrokerModule } from '../setup/broker/broker.module'
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {AgGridModule} from "ag-grid-ng2";
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {DataTableModule} from "primeng/components/datatable/datatable";
import {SharedModule} from "primeng/components/common/shared";
import {EditorModule} from "primeng/components/editor/editor";
import {MaterialModule, MdToolbarModule, MdProgressSpinnerModule} from "@angular/material";
import {FlexLayoutModule} from "@angular/flex-layout";
import {NotificationModule} from "../core/services/notification/notification.module";
import {FormModule} from "../core/ui-component/form/form.module";
import {InputComboModule} from "../core/ui-component/input-combo/input-combo.module";
import {DataGridModule} from "../core/ui-component/data-grid/data-grid.module";
import {MainRoutingModule} from "./main.routes";
import {ToolbarModule} from "../core/ui-component/toolbar/toolbar.module";

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    AgGridModule.withComponents(['enableColResize']),
    HttpModule,
    FormsModule,
    DataTableModule,SharedModule, EditorModule,
    MaterialModule,
    MdToolbarModule,
    MdProgressSpinnerModule,
    FlexLayoutModule,
    NotificationModule,
    FormModule,
    InputComboModule,
    DataGridModule,
    BrokerModule,
    MainRoutingModule,
    ToolbarModule
  ],
  declarations: [
    MainComponent,
    LogoutComponent,
    GridDirective,
    ProfileComponent
  ]
})
export class MainModule { }
