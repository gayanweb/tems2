import {Component, OnInit, ViewEncapsulation, HostBinding} from '@angular/core';
import {Http} from '../core/services/http';

import {NotificationService} from '../core/services/notification/notification.service'
import {slideInDownAnimation} from "../app.animations";


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  providers: [Http, NotificationService],
  // animations: [slideInDownAnimation]
})
export class MainComponent implements OnInit {
  // @HostBinding('@routeAnimation') routeAnimation = true;
  private menuItems = [
    {
      header: 'Setup',
      items: [
        {link: '/main', icon: 'dashboard', display: 'Main'},
        {link: 'broker', icon: 'group', display: 'Brokers'},
        {link: 'invoice', icon: 'folder', display: 'Invoice'}
      ]
    }
    ,{
      header: 'Setup',
      items: [
        {link: 'broker', icon: 'folder', display: 'Brokers'},
        {link: 'broker', icon: 'folder', display: 'Brokers'},
        {link: 'broker', icon: 'folder', display: 'Brokers'}
      ]
    }
    ,{
      header: 'Setup',
      items: [
        {link: 'broker', icon: 'folder', display: 'Brokers'},
        {link: 'broker', icon: 'folder', display: 'Brokers'},
        {link: 'broker', icon: 'folder', display: 'Brokers'}
      ]
    }
  ];

  constructor() {}

  ngOnInit() {}

}
