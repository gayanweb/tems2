/**
 * Created by gayanyapa on 2/13/17.
 */
import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import {BrokerComponent} from "../setup/broker/broker.component";
import {MainComponent} from "./main.component";
import {InvoiceComponent} from "../purchase/invoice/invoice/invoice.component";
import {ToolbarModule} from "../core/ui-component/toolbar/toolbar.module";
const appRoutes: Routes = [
  {path: 'main', component: MainComponent, children: [
    { path: '', redirectTo: 'broker', pathMatch: 'full' },
    { path: 'broker', component: BrokerComponent },
    { path: 'invoice', component: InvoiceComponent },
  ]},
  // {path: 'broker', component: BrokerComponent},

];
@NgModule({
  imports: [
    RouterModule.forChild(appRoutes),
    ToolbarModule,
  ],
  exports: [
    RouterModule
  ]
})
export class MainRoutingModule {}
