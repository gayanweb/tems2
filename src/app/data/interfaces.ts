/**
 * Created by gayanyapa on 2/21/17.
 */
export interface Broker{
  id: number,
  name: string,
  address: string,
  telephone: string,
  code: string,
  freeStorage: number
}
